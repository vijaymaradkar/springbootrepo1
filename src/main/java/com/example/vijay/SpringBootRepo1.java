package com.example.vijay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootRepo1 {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootRepo1.class, args);
	}
}
